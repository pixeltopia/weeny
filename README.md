# Weeny Machine #

A tiny virtual machine.

## What is this repository for? ##

This repository contains a specification for an imaginary 8-bit machine called the Weeny Machine, an assembler, and a bytecode interpeter.

## How do I get set up? ##

There's an assembler, written in ruby. It's been written using ruby 2.6.3.

## Contribution guidelines ##

The assembler uses MiniTest for testing.
