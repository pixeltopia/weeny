# Weeny Machine Mark 1 #

## Introduction ##

The Weeny Machine Mark 1 (referred to as WM/1) is a software emulated machine designed to run small programs and games. It supports 8-bit and 16-bit signed and unsigned integer arithmetic and comparisons. Signed numbers are represented in 2's compliment.

## Machine Images ##

WM/1 has a 64kb address space, with a 16-bit addresses specified in the hexadecimal number range $0000-$FFFF. Registers are included in this address space, so the entire machine state can be restored from a file. As registers are part of addressable memory, it is possible for a program to directly manipulate these values but they're not intended to be accessed.

Assembling a program produces a machine state with pointers set to an initial state; all 16-bit values and pointers are stored in big-endian format.

## Instructions Overview ##

WM/1 has instructions for performing: arithmetic operations; loading and storing data; incrementing and decrementing values; manipulating a stack; comparing values; bitwise manipulations; calling and returning from subroutines; branching and jumping; and switching the CPU mode.

WM/1 has some special instructions that are included to make certain things easier for the programmer:

- two forms of comparison operation, `CMS` (compare signed) and `CMU` (compare unsigned),
- two stack operations `PHM` (push memory) and `PLM` (pull memory) that are intended to be used to store and restore memory that can be used for parameters and local variables,
- two mode operations `SEX` (set extended mode) and `REX` (reset extended mode) to switch between extended mode (16-bit) and regular mode (8-bit) to make it easier to do arithmetic on 16-bit values.

WM/1 also supports some useful addressing modes:

- ranged addressing, which specifies a base address and an unsigned length to be used to store and restore memory to the stack,
- indirect addressing, which can be used to iterate over memory ranges.

## Registers ##

The machine has four registers; two 16-bit pointers that contain the address of memory locations, an 8-bit processor flag and a 16-bit accumulator:

| symbol | name                | size   |
|:------:|:--------------------|-------:|
|   IP   | instruction pointer | 16-bit |
|   SP   | stack pointer       | 16-bit |
|   P    | flags `[_____BZX]`  |  8-bit |
|   A    | accumulator         | 16-bit |

## Processor Flags ##

There are three processor flags, `B`, `Z` and `X`. `B` is set during comparison, subtraction and when restoring the process flags from the stack. `Z` is set whenever an operation causes the accumulator to be zero. `X` is set and cleared by user code and it switches operations to 16-bit (extended mode) when set, and 8-bit (regular mode) when cleared.

## Memory Map : Registers and Code ##

The processor registers are stored at the high end of the address range. Executable code starts at the low end, $0000.

The stack starts at $FFF8. The stack pointer decrements each time a byte is pushed and it increments when a value is pulled off. The stack pointer _doesn't_ point at the item at the _top_ of the stack, but the place where the _next_ item will be pushed. When pushing an item, the item is stored at the pointer address, _then_ the pointer is decremented. When pulling, the pointer is decremented, _then_ the value is read.

_There are no guarantees about memory protection, so be careful not to underflow the stack and accidentally overwrite other registers._

```
+-------+---------+------+
| $FFFF | IP (lo) | #$00 |
| $FFFE | IP (hi) | #$00 | ---- IP initially contains the value $0000
+-------+---------+------+
| $FFFD | SP (lo) | #$F8 |
| $FFFC | SP (hi) | #$FF | ---+
+-------+---------+------+    |
| $FFFB | P       |      |    |
+-------+---------+------+    | SP initially contains the value $FFF8
| $FFFA | A  (lo) |      |    |
| $FFF9 | A  (hi) |      |    |
+-------+---------+------|    |
| $FFF8 |         |      | <--+
+-------+---------+------+

... code & data ...

+-------+---------+
| $0000 | code    |
+-------+---------+
```

## Instructions and Addressing Modes ##

There are 31 instructions and 5 addressing modes; only certain addressing modes are supported by some operations. The opcode encoding contains both the operation and addressing mode; the high 5 bits (allowing for 32 possible instructions) are used for the operation, and the low 3 bits (allowing for 8 possible modes) are used for the addressing mode:

| 0 - 31 | 0 - 7 |
|:------:|:-----:|
| `11111`| `111` |

The addressing modes and the notation used to show these modes are as follows:

| bit pattern | mode name      | notation | description                                    |
|:-----------:|:---------------|:---------|:-----------------------------------------------|
|    `000`    | implicit       | non      | no operand specified                           |
|    `001`    | absolute       | adr      | 16-bit address                                 |
|    `010`    | absolute range | adr,len  | 16-bit address then an 8-bit unsigned value    |
|    `011`    | immediate      | val      | 16-bit or 8-bit signed/unsigned value          |
|    `100`    | indirect       | (adr)    | 16-bit address of another 16-bit address       |

The instruction encoding, mnemonic and supported addressing modes are as follows:

| code | mnemonic | addressing modes  | description                              |
|:----:|:--------:|:------------------|:-----------------------------------------|
|  00  |  `HLT`   | non               | halt                                     |
|  01  |  `LDA`   | adr / (adr) / val | load accumulator                         |
|  02  |  `STA`   | adr / (adr)       | store accumulator                        |
|  03  |  `AND`   | adr / (adr) / val | bitwise and                              |
|  04  |  `ORA`   | adr / (adr) / val | bitwise or                               |
|  05  |  `XOR`   | adr / (adr) / val | bitwise exclusive or                     |
|  06  |  `LSL`   | adr / (adr) / non | logical shift left                       |
|  07  |  `LSR`   | adr / (adr) / non | logical shift right                      |
|  08  |  `ASR`   | adr / (adr) / non | arithmetic shift right (sign preserving) |
|  09  |  `ROL`   | adr / (adr) / non | rotate left                              |
|  0A  |  `ROR`   | adr / (adr) / non | rotate right                             |
|  0B  |  `ADD`   | adr / (adr) / val | add                                      |
|  0C  |  `SUB`   | adr / (adr) / val | subtract                                 |
|  0D  |  `INC`   | adr / (adr) / non | increment                                |
|  0E  |  `DEC`   | adr / (adr) / non | decrement                                |
|  0F  |  `CMS`   | adr / (adr) / val | compare signed                           |
|  10  |  `CMU`   | adr / (adr) / val | compare unsigned                         |
|  11  |  `BEQ`   | adr               | branch if equal                          |
|  12  |  `BNE`   | adr               | branch if not equal                      |
|  13  |  `BLT`   | adr               | branch if less than                      |
|  14  |  `JMP`   | adr               | jump                                     |
|  15  |  `JSR`   | adr               | jump to subroutine                       |
|  16  |  `RTS`   | non               | return from subroutine                   |
|  17  |  `PHA`   | non               | push accumulator                         |
|  18  |  `PLA`   | non               | pull accumulator                         |
|  19  |  `PHP`   | non               | push flags                               |
|  1A  |  `PLP`   | non               | pull flags                               |
|  1B  |  `PHM`   | adr,len           | push memory                              |
|  1C  |  `PLM`   | adr,len           | pull memory                              |
|  1D  |  `SEX`   | non               | set extended mode                        |
|  1E  |  `REX`   | non               | reset extended mode                      |

Examples of the binary encoding of instructions and different addressing modes are as follows:

| notation | assembly syntax   | binary encoding                                  |
|:---------|:------------------|:-------------------------------------------------|
| non      | `HLT`             | `00000000`                                       |
| adr      | `LDA $F0F0`       | `00001001  11110000 11110000`                    |
| adr,len  | `PHM $F0F0,#$0F`  | `11101010  11110000 11110000  00001111`          |
| val      | `LDA #$0F`        | `00001011  00001111`                             |
| (adr)    | `LDA ($F0F0)`     | `00001100  11110000 11110000`                    |

## Addressing Modes: Implicit ##

Implicit does not specify any operand. Operations have an implicit register that they operate on.

## Addressing Modes: Absolute ##

Absolute specifies a single address containing the location of a value to look up. It _is not affected_ by the extended mode flag, so the second operand must always be a 16-bit unsigned value.

## Addressing Modes: Absolute Range ##

Ranged addressing is used exclusively with the `PHM` and `PLM` instructions. It _is not affected_ by the extended mode flag, so the second operand must always be an 8-bit unsigned value.

## Addressing Modes: Immediate ##

Immediate addressing specifies a constant value. It _is affected_ by the the extended mode flag. Any specified constants should be the correct size for the mode, e.g. `#$00` in 8-bit regular mode or `##$0000` in 16-bit extended mode (see the syntax guide for the meaning of the `##`.)

## Addressing Modes: Indirect ##

Indirect addressing is used to implement _pointers_. When you specify an address, a pair of values are read, concatenated together in big-endian format, and interpreted as another address which is where the final value is loaded from. It _is not affected_ by the extended mode flag, so the second operand must always be a 16-bit unsigned value.

So in the example `LDA ($F000)`, if `$F000` contains the value `#$AB` and `$F001` contains the value `#$CD` then the accumulator will be loaded with the contents of `$ABCD`:

```
+-------+-----------+
| $F001 | #$CD (lo) |
| $F000 | #$AB (hi) | ---+ $F000 contains #$AB, $F001 contains #$CD
+-------+-----------+    | #$AB #$CD concatenated together is $ABCD
...                      |
+-------+-----------+    |
| $ABCD | #$FF      | <--+ point here
+-------+-----------+
```

This pointer can be decremented with the `DEC` instruction:

```
; decrements the value at address $F000 in 16-bit extended mode
SEX
DEC $F000
REX
```

Which would decrement the value from `#$ABCD` to `#$ABCC`. Executing the instruction `LDA ($F000)` again now causes the previous memory location to be loaded into memory.

```
+-------+-----------+
| $F001 | #$CC (lo) |
| $F000 | #$AB (hi) | ---+ $F000 contains #$AB, $F001 contains #$CC
+-------+-----------+    | #$AB #$CC concatenated together is $ABCC
...                      |
+-------+-----------+    |
| $ABCD | #$FF      |    | did point here before decrementing
+-------+-----------+    |
| $ABCC | #$EE      | <--+ now point here
+-------+-----------+
```

In this way, indirect mode and extended mode allow for pointers and pointer arithmetic.

## Assembly Syntax ##

WM/1 assembly language programs are text files that are a series of lines. A line can take one of four forms:

- an instruction,
- a symbol declaration and assignment,
- a label,
- or a data definition directive.

Comments can also be suffixed to lines.

Blank lines are ignored.

### Assembly Syntax: Comments ###

Lines can contain comments, anything after the `;` character up to the end of the line is ignored.

### Assembly Syntax: Addresses ###

Addresses are always prefixed with a `$` and always have four hexadecimal characters, written big-endian with the high byte on the left, representing a 16-bit unsigned value for a position in memory:

```
    $0001 - represents decimal address 1
    $FFFF - represents decimal address 65535
```

### Assembly Syntax: Value Constants ###

Value constants are always prefixed with a `#` and represent either an 8-bit or 16-bit value; they can be written in either hexadecimal, decimal, binary or as an ASCII character.

In extended mode, constant values must be prefixed with an extra `#`, so have a `##` prefix. ASCII characters in extended mode result in a zero-padded value to be encoded.

Values can be interpreted either unsigned or signed, depending on the needs of the program, e.g. the bit pattern for the value `128` can either represent the unsigned decimal value `128`, but it can also represent the signed decimal value `-128` in 2's compliment when doing signed comparisons.

8-bit regular mode:

| syntax       | mode        | value       |
|:-------------|:------------|:------------|
| `#$80`       | hexadecimal | 128 or -128 |
| `#128`       | decimal     | 128 or -128 |
| `#%10000000` | binary      | 128 or -128 |
| `#-128`      | decimal     | 128 or -128 |
| `#-1`        | decimal     | 255 or -1   |
| `#255`       | decimal     | 255 or -1   |
| `#'A`        | ASCII value | 65 (`#$41`) |

16-bit extended mode:

| syntax                | mode        | value           |
|:----------------------|:------------|:----------------|
| `##$8000`             | hexadecimal | 32768 or -32768 |
| `##32768`             | decimal     | 32768 or -32768 |
| `##%1000000000000000` | binary      | 32768 or -32768 |
| `##-32768`            | decimal     | 32768 or -32768 |
| `##-1`                | decimal     | 65535 or -1     |
| `##65535`             | decimal     | 65535 or -1     |
| `##'A`                | ASCII value | 65 (`#$0041`)   |

### Assembly Syntax: Instruction ###

An instruction must be indented with at least one whitespace character, followed by the instruction opcode mnemonic and any operands.

There must be a single space between the mnemonic and the operarands. Operands are separated by the `,` character _without_ whitespace, e.g.

```
    ABC $0000,$0000
```

The format of the operands (after resolving any symbols or labels) is used to work out the addressing mode.

In the following examples, `<SYM>` is a placeholder which would be replaced with either a _symbol_ or a _label_.

### Assembly Syntax: Implicit Addressing ###

The implicit addressing mode doesn't have any operands:

```
    ABC
```

### Assembly Syntax: Absolute Addressing ###

The absolute addressing mode has a single address:

```
    ABC $0000
    ABC <SYM>
```

### Assembly Syntax: Absolute Range Addressing ###

The absolute range addressing mode has an address and an unsigned value:

```
    ABC $0000,#$80          ; value is hexadecimal (128)
    ABC $0000,#128          ; value is decimal     (128)
    ABC $0000,#%10000000    ; value is binary      (128)
    ABC $0000,#'A           ; value is ASCII value 65 - allowed but not recommended
    ABC $0000,#<SYM>
    ABC <SYM>,#$80
    ABC <SYM>,#128
    ABC <SYM>,#%10000000
    ABC <SYM>,#'A
    ABC <SYM>,#<SYM>
```

### Assembly Syntax: Immediate Addressing ###

The immediate addressing mode has either an 8-bit or 16-bit value constant.

Decimals can be prefixed with a negative sign.

Values can be interpreted either unsigned or signed, depending on the needs of the program.

8-bit regular mode:

```
    ; values interpreted as unsigned or signed

    ABC #$80        ; value is hexadecimal (128 or -128)
    ABC #128        ; value is decimal     (128 or -128)
    ABC #%10000000  ; value is binary      (128 or -128)
    ABC #-128       ; value is decimal     (128 or -128)
    ABC #-1         ; value is decimal     (255 or -1)
    ABC #255        ; value is decimal     (255 or -1)
    ABC #$FF        ; value is hexadecimal (255 or -1)
    ABC #'A         ; value is ASCII value 65
    ABC #<SYM>
```

16-bit extended mode:

```
    ; values interpreted as unsigned or signed

    ABC ##$8000             ; value is hexadecimal (32768 or -32768)
    ABC ##32768             ; value is decimal     (32768 or -32768)
    ABC ##%1000000000000000 ; value is binary      (32768 or -32768)
    ABC ##-32768            ; value is decimal     (32768 or -32768)
    ABC ##-1                ; value is decimal     (65535 or -1)
    ABC ##65535             ; value is decimal     (65535 or -1)
    ABC ##$FFFF             ; value is hexadecimal (65535 or -1)
    ABC ##'A                ; value is ASCII value 65
    ABC ##<SYM>
```

### Assembly Syntax: Indirect Addressing ###

The indirect addressing mode specifies an address inside parenthesis characters `(...)`:

```
    ABC ($8080)
    ABC (<SYM>)
```

### Assembly Syntax: Symbol Declaration and Assignment ###

Symbol names must start with either an alphabetical character or an underscore; they can have any number of alphanumeric or underscore characters following the first character. Alphabetical characters can be mixed case. Symbol names _cannot_ contain whitespace. Symbol names are case-sensitive, so `a1` is a different symbol to `A1`.

After the symbol name there is the assignment character `=` followed by either an address or a value constant.

Whitespace characters _are allowed_ in-between symbols, the assignment character, and expressions, but the first character _cannot_ be whitespace.

Symbols are mechanically replaced, arithmetic expressions are not supported.

```
A = $0001
B = $0002
C = 1

main:
    LDA A    ; absolute
    LDA ##A  ; immediate (extended mode)
    LDA (A)  ; indirect
    PHM A,#C ; absolute range
```

### Assembly Syntax: Labels ###

Label names follow the specification for symbol names, the last character of a label must be `:` without any whitespace in-between the label.

There must be at least one label, this must be called `main` and it must be the first label of the program.

Labels are meant to be used for two purposes:

- branching, subroutine calls and performing jumps,
- and for loading user-defined program data.

This example shows the `main` label, a subroutine call and an infinite loop:

```
main:
    JSR subroutine
loop:
    JMP loop

subroutine:
    RTS
```

### Assembly Syntax: Labels as symbolic constants ###

There's a special syntax for referring to labels as constants. This allows a program to copy data embedded with the program into different areas of memory at runtime (e.g. game data.) The assembler puts the final assembled position of the label into the code.

This example program shows the assembled positions of code and data, and copies two bytes of data from within the assembled code (labelled `data:`) into memory locations `$ABCD` and `$ABCE`:

```
|       |  pointerA    = $8000
|       |  pointerB    = $8002
|       |  destination = $ABCD
|       |
|       |  main:
| $0000 |      SEX               ; 1 byte
| $0001 |      LDA ##data        ; 3 bytes : loads ##$0023 into $8000-$8001
| $0004 |      STA pointerA      ; 3 bytes
| $0007 |      LDA ##destination ; 3 bytes : loads ##$ABCD into $8002-$8003
| $000A |      STA pointerB      ; 3 bytes
| $000D |      REX               ; 1 byte
| $000E |      LDA (pointerA)    ; 3 bytes : LDA ($8000) which is LDA $0023
| $0011 |      STA (pointerB)    ; 3 bytes : STA ($8002) which is STA $ABCD
| $0014 |      SEX               ; 1 byte
| $0015 |      INC pointerA      ; 3 bytes : INC $8000 ($0023 + 1 is $0024)
| $0018 |      INC pointerB      ; 3 bytes : INC $8002 ($ABCD + 1 is $ABCE)
| $001B |      REX               ; 1 byte
| $001C |      LDA (pointerA)    ; 3 bytes : LDA ($8000) which is LDA $0024
| $001F |      STA (pointerB)    ; 3 bytes : LDA ($8002) which is STA $ABCE
| $0022 |      HLT               ; 1 byte
|       |  data:
| $0023 |      .bytes 00         ; 1 byte
| $0024 |      .chars A          ; 1 byte
```

### Assembly Syntax: Data definition directive ###

A data definition directive is used to insert constant values into the assembled code. This can be used to insert user-defined values (bytes, numbers, text characters) that the program can then make use of (or copy.)

Data definition directive lines end with a newline character.

Bytes can be inserted by writing `.bytes` then a space, then some hexadecimal characters (each one represents a nybble) and there must be an even number of characters. Spaces can be used to visually represent different types of value (e.g. 8-bit vs 16-bit values) but spaces are ignored when assembling the final bytes.

```
    .bytes 0 1 0 2 0 3 A 0 B 0 C 0 D 0 E 0 F 0 F F
    .bytes 01 02 03 A0 B0 C0 D0 E0 F0 FF
    .bytes 0102 03A0 B0C0 D0E0 F0FF
    .bytes 010203A0B0C0D0E0F0FF
```

ASCII characters can be inserted by writing `.chars` then a space, then some characters. The characters must be in the 7-bit ASCII range, and special characters can be inserted using the backslash `\` character, e.g. `\n` for newline, `\t` for tab and `\\` for the backslash character. Spaces _are_ included for ASCII characters. All characters are bytes.

```
    .chars good things come in small packages
```

## Subroutines, calling convention and local variables ##

The `JSR` and `RTS` instructions are used to perform jumps. The `JSR` instruction pushes the address of the next instruction to execute (using the current value of the `IP` register) and then jumps to an absolute position in memory to execute the next instruction. The `RTS` instruction pulls the address back off the stack and then jumps back.

Subroutine parameters and results can be stored using _psuedo registers_ rather than passing via the stack, where a pseudo register is just a user-defined global location in memory. Likewise another global location in memory can be used for local variables. If the state of parameters needs to be kept in-between subroutine calls, these should also be pushed and pulled off the stack. This area of memory could be thought of like being a scratchpad with values written on it, where pages are placed on and taken off the stack when calling subroutines.

To help the programmer store and restore the scratchpad, there's two instructions, `PHM` that takes a memory location and a number of bytes to push, and `PLM` that takes a memory location and a number of bytes to pull off and copy back. With `PHM` and `PLM`, the number of bytes is _always_ an 8-bit unsigned constant value, regardless of the state of the extended mode flag.

The stack is designed to be used as a temporary place to store local variables, temporary values and return addresses. The `RTS` instruction expects the return address to be on the top of the stack, so any temporary pushed values must be removed to ensure the instruction jumps back to the correct place.

An example of a program that pushes local variables, sets and pushes parameters and calls two subroutines is as follows:

```
; a scratchpad memory map, with some
; addresses somewhere below the stack

; result
RESULT = $8000

; parameters
PARAMS = $8001 ; first parameter address
PARAM1 = $8001
PARAM2 = $8002
PARAM3 = $8003
PARAM4 = $8004

; locals
LOCALS = $8005 ; first local address
LOCAL1 = $8005
LOCAL2 = $8006

main:
    LDA #'Z
    STA LOCAL1    ; local 1 is 'Z'
    LDA #'X
    STA LOCAL2    ; local 2 is 'X'
    LDA #3
    STA PARAM1    ; param 1 is 3
    LDA #4
    STA PARAM2    ; param 2 is 4
    PHM LOCALS,#2 ; push local bytes
    PHP           ; push flags
    PHA           ; push accumulator
    JSR sub1      ; jump
    PLA           ; pull accumulator
    PLP           ; pull flags
    PLM LOCALS,#2 ; pull local bytes
    HLT           ; result is 7

; stores passed parameters, adds them, then returns result

sub1:
    PHM PARAMS,#2 ; push param bytes
    JSR sub2      ; jump
    PLM PARAMS,#2 ; pull param bytes
    LDA PARAM1    ; load param 1
    ADD PARAM2    ; adds param 2
    STA RESULT    ; store result
    RTS           ; return

; clears all the scratchpad

sub2:
    LDA #0
    STA PARAM1    ; param 1 is 0
    STA PARAM2    ; param 2 is 0
    STA PARAM3    ; param 3 is 0
    STA PARAM4    ; param 4 is 0
    STA LOCAL1    ; local 1 is 0
    STA LOCAL2    ; local 2 is 0
    RTS           ; return
```

## Memory-mapped I/O ##

_To be defined..._

## Appendix A: Numbers (decimal, binary, hexadecimal, signed and unsigned 2's compliment) ##

_To be defined..._
